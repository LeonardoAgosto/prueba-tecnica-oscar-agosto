import { createApp } from 'vue'
import {createRouter, createWebHashHistory} from 'vue-router'
import App from './App.vue'
import LoginView from '../src/components/LoginView'
import HomeView from '../src/components/HomeView'
import CartView from '../src/components/CartView'
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios';


const routes = [
    {
        path:'/', component: LoginView
    },
    {
        path:'/home', component: HomeView
    },
    {
        path:'/cart', component: CartView
    },
]

// Creacion de rutas de vue

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

// instancias
const app = createApp(App)

app.config.globalProperties.$axios = axios

app.use(router)

app.mount('#app')